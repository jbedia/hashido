/*global $*/

var ISH = ISH || {};

// game constants
$.extend(ISH, {
    constants: {
        width: 12,
        height: 8,
        colours: ['red', 'orange', 'yellow', 'green', 'blue', 'violet'],
        symbols: ['circle', 'crescent', 'triangle', 'diamond', 'cross', 'star'],
        multiples: 2,
        bonuses: [25, 50, 100, 200, 400, 600, 800, 1000, 5000, 10000, 25000, 50000],
        openingPositions: [0, 11, 41, 54, 84, 95]
    },
    gameStates: {
        inProgress: 'game-in-progress',
        overNoMoves: 'game-over-no-moves',
        overComplete: 'game-over-complete'
    }
});

// constructor function for the game controller
ISH.Game = function (board, boardView, pouch, pouchView, status, statusView, tutorialView, Tile, constants, options) {
    'use strict';
    
    var i, j, k, l, rnd, tile,
        foundColours = [],
        foundSymbols = [],
        originalTiles = [],
        tiles = [],
        that = this;
    this.options = options;
    
    // handle a move being made
    this.makeMove = function (index, board, pouch, status) {
        board.setTile(pouch.takeNextTile(), index);
        this.addScore(index, board, status);
        this.calculateValidMoves(board, pouch);
        this.isGameOver(board, pouch, status);
    };
    
    // increment the score for the pieces just laid down
    this.addScore = function (index, board, status) {
        var points, scoreMultiplier, surroundingTiles;
        
        scoreMultiplier = Math.pow(2, status.fourWays);
        surroundingTiles = board.getSurroundingTiles(index);
        
        if (!board.isScoringTile(index) || surroundingTiles.length === 0) {
            points = 0;
        } else {
            points = Math.pow(2, surroundingTiles.length - 1) * scoreMultiplier;
        }
        if (surroundingTiles.length === 4) {
            points += constants.bonuses[status.fourWays];
            status.fourWays += 1;
        }
        status.lastScore = points;
        status.setScore(status.getScore() + points);
        
        return points;
    };
    
    // for every space on the board, set where the next tile can and can't be placed 
    this.calculateValidMoves = function (board, pouch) {
        var i;
        for (i = 0; i < board.getAllValid().length; i += 1) {
            board.setValid(this.isValidMove(i, board, pouch), i);
        }
        board.trigger('change');
    };
    
    // for a given space on the board, could the next tile be placed there?
    this.isValidMove = function (index, board, pouch) {
        var i, colourMatches, symbolMatches, activeTile, surroundingTiles;
        
        activeTile = pouch.getNextTile();
        surroundingTiles = board.getSurroundingTiles(index);
        colourMatches = 0;
        symbolMatches = 0;
        
        // if there are no more tiles, no more moves can be made
        if (!activeTile) {
            return false;
        }
        
        // if this cell is occupied, obviously it's not valid
        if (board.getTile(index)) {
            return false;
        }
        
        // if any tiles match by neither symbol nor colour, reject the cell
        for (i = 0; i < surroundingTiles.length; i += 1) {
            if (activeTile.colour !== surroundingTiles[i].colour &&
                    activeTile.symbol !== surroundingTiles[i].symbol) {
                return false;
            }
            if (activeTile.colour === surroundingTiles[i].colour) {
                colourMatches += 1;
            }
            if (activeTile.symbol === surroundingTiles[i].symbol) {
                symbolMatches += 1;
            }
        }
        
        // special placement validation, depending on number of adjoining tiles
        // note for cases 1 and 3 that we already know all tiles match at least in colour or symbol
        switch (surroundingTiles.length) {
        case 0:
            return false;
        case 1:
            return true;
        case 2:
        case 3:
            return colourMatches >= 1 && symbolMatches >= 1;
        case 4:
            return colourMatches >= 2 && symbolMatches >= 2;
        }
    };
    
    this.isGameOver = function (board, pouch, status) {
        if (board.countValid() === 0) {
            status.setGameState(ISH.gameStates.overNoMoves);
        }
        if (pouch.getTilesLeft() === 0) {
            status.setGameState(ISH.gameStates.overComplete);
        }
    };
    
    this.reset = function () {
        boardView.unbindAll();
        statusView.unbindAll();
        ISH.init(this.options);
    };
    
    // mediate events
    board.on('change', boardView.render);
    pouch.on('change', pouchView.render);
    status.on('change', statusView.render);
    boardView.on('place', function (index) {
        that.makeMove(index, board, pouch, status);
    });
    statusView.on('newGame', function () {
        that.reset();
    });
    statusView.on('showTutorial', tutorialView.show);
    
    // initialisation
    
    // generate pieces, shuffle them, and put them in the pouch
    // ensuring that the first 6 cover all permutations
    for (i = 0; i < constants.colours.length; i += 1) {
        for (j = 0; j < constants.symbols.length; j += 1) {
            for (k = 0; k < constants.multiples; k += 1) {
                l = (i * constants.colours.length * constants.multiples) + (j * constants.multiples) + k;
                originalTiles[l] = new Tile(constants.colours[i], constants.symbols[j]);
            }
        }
    }
    
    while (originalTiles.length) {
        rnd = Math.floor(Math.random() * originalTiles.length);
        tile = originalTiles.splice(rnd, 1)[0];
        if (!foundColours[tile.colour] && !foundSymbols[tile.symbol]) {
            tiles.unshift(tile);
            foundColours[tile.colour] = foundSymbols[tile.symbol] = true;
        } else {
            tiles.push(tile);
        }
    }
    
    pouch.setAllTiles(tiles);
    
    // lay down the initial arrangement of tiles
    for (i = 0; i < constants.openingPositions.length; i += 1) {
        board.setTile(pouch.takeNextTile(), constants.openingPositions[i]);
    }
    this.calculateValidMoves(board, pouch);
    
};

// initializes the game
ISH.init = function (options) {
    'use strict';
    
    this.board = new this.Board(this.constants.width, this.constants.height);
    this.boardView = new this.BoardView(this.board, options.boardElement, options.boardTemplate);
    
    this.pouch = new this.Pouch();
    this.pouchView = new this.PouchView(this.pouch, options.pouchElement, options.pouchTemplate);
    
    this.status = new this.Status();
    this.statusView = new this.StatusView(this.status, options.statusElements, options.statusTemplate);
    
    this.tutorialView = new this.TutorialView(options.tutorialElement);
    
    this.game = new this.Game(this.board, this.boardView,
                              this.pouch, this.pouchView,
                              this.status, this.statusView,
                              this.tutorialView,
                              this.Tile,
                              this.constants,
                              options);
    
    return this;
};