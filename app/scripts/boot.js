/*global document, $, FastClick*/

var ISH = ISH || {};

$(document).ready(function () {
    'use strict';
    
    FastClick.attach(document.body);
    
    ISH.init({
        boardElement: $('#board'),
        boardTemplate: $('#board-template'),
        pouchElement: $('#pouch'),
        pouchTemplate: $('#pouch-template'),
        statusElements: {
            panel: $('#status'),
            body: $('body'),
            controls: $('#controls'),
            footer: $('#page-footer')
        },
        statusTemplate: $('#status-template'),
        tutorialElement: $('#tutorial')
    });
});