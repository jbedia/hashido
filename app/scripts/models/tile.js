var ISH = ISH || {};

// constructor function for a tile model
ISH.Tile = function (colour, symbol) {
    'use strict';
    
    this.colour = colour;
    this.symbol = symbol;
};