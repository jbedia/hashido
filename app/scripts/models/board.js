/*global $, EventEmitter*/

var ISH = ISH || {};

// constructor function for the board model
// creates an object with an empty spaces array of the right length
// and an empty valid array (of possible moves) of the right length
ISH.Board = function (width, height) {
    'use strict';
    
    var lastMove,
        spaces = [],
        valid = [];
    spaces.length = width * height;
    valid.length = width * height;
    
    this.getTile = function (index) {
        return spaces[index];
    };
    
    this.setTile = function (tile, index) {
        spaces[index] = tile;
        lastMove = index;
        return tile;
    };
    
    this.getAllTiles = function () {
        return spaces;
    };
    
    this.getSurroundingTiles = function (index) {
        var i, newTiles, tiles;
        tiles = [
            spaces[this.cellUp(index)],
            spaces[this.cellRight(index)],
            spaces[this.cellDown(index)],
            spaces[this.cellLeft(index)]
        ];
        newTiles = [];
        for (i = 0; i < 4; i += 1) {
            if (tiles[i]) {
                newTiles.push(tiles[i]);
            }
        }
        return newTiles;
    };
    
    this.getValid = function (index) {
        return valid[index];
    };
    
    this.setValid = function (validity, index) {
        valid[index] = validity;
        return validity;
    };
    
    this.getAllValid = function () {
        return valid;
    };
    
    this.countValid = function () {
        var i, count;
        count = 0;
        for (i = 0; i < valid.length; i += 1) {
            if (valid[i]) {
                count += 1;
            }
        }
        return count;
    };
    
    this.getLastMove = function () {
        return lastMove;
    };
    
    this.cellUp = function (index) {
        var newIndex = index - width;
        return (newIndex >= 0) && newIndex;
    };
    
    this.cellRight = function (index) {
        return ((index + 1) % width !== 0) && (index + 1);
    };
    
    this.cellDown = function (index) {
        var newIndex = index + width;
        return (newIndex < width * height) && newIndex;
    };
    
    this.cellLeft = function (index) {
        return (index % width !== 0) && (index - 1);
    };
    
    // moves should only score if they are not at the edge of the board
    this.isScoringTile = function (index) {
        return this.cellUp(index) &&
            this.cellRight(index) &&
            this.cellDown(index) &&
            this.cellLeft(index);
    };
};

$.extend(true, ISH.Board.prototype, EventEmitter.prototype);