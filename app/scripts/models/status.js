/*global $, EventEmitter*/

var ISH = ISH || {};

// constructor function for the status model
ISH.Status = function () {
    'use strict';
    
    var score = 0,
        gameState = ISH.gameStates.inProgress;
    this.lastScore = 0;
    this.fourWays = 0;
    
    this.getScore = function () {
        return score;
    };
    
    this.setScore = function (newScore) {
        score = newScore;
        this.trigger('change');
    };
    
    this.getGameState = function () {
        return gameState;
    };
    
    this.setGameState = function (state) {
        gameState = state;
        this.trigger('change');
    };
};

$.extend(ISH.Status.prototype, EventEmitter.prototype);
