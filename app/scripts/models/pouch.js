/*global $, EventEmitter*/

var ISH = ISH || {};

// constructor function for the pouch model
// generates a pouch containing all pieces in random order
// TODO: is that game logic, should it be in the controller?
ISH.Pouch = function () {
    'use strict';
    
    var tiles = [];
    
    this.setAllTiles = function (newTiles) {
        tiles = newTiles;
        this.trigger('change');
    };
    
    this.getNextTile = function () {
        return tiles[0];
    };
    
    this.getAllTiles = function () {
        return tiles;
    };
    
    this.takeNextTile = function () {
        var tile;
        tile = tiles.shift();
        this.trigger('change');
        return tile;
    };
    
    this.getTilesLeft = function () {
        return tiles.length;
    };
};

$.extend(ISH.Pouch.prototype, EventEmitter.prototype);