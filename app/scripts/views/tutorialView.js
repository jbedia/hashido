/*global $*/

var ISH = ISH || {};

ISH.TutorialView = function (element) {
    'use strict';
    
    element.load(element.data('url'));
    
    this.show = function () {
        element.removeClass('hidden');
    };
    
    this.hide = function (e) {
        e.preventDefault();
        element.addClass('hidden');
    };
    
    this.navigate = function (e) {
        e.preventDefault();
        element.find('.tutorial-panel').hide();
        element.find($(this).attr('href')).show();
    };
    
    element.on('click', '.previous, .next', this.navigate);
    element.on('click', '.close', this.hide);
    
};