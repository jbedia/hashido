/*global $, doT, EventEmitter*/

var ISH = ISH || {};

ISH.BoardView = function (model, element, template) {
    'use strict';
    
    var that = this;
    this.element = element;

    this.render = function () {
        var templateFn;
        templateFn = doT.template(template.html());
        element.html(templateFn({
            tiles: model.getAllTiles(),
            valid: model.getAllValid(),
            lastMove: model.getLastMove()
        }));
    };
    
    this.unbindAll = function () {
        element.off('click');
    };
    
    element.on('click', '.valid', function () {
        that.trigger('place', [$(this).index()]);
    });
};

$.extend(ISH.BoardView.prototype, EventEmitter.prototype);