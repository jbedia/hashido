/*global $, doT, EventEmitter*/

var ISH = ISH || {};

ISH.StatusView = function (model, elements, template) {
    'use strict';
    
    var that = this;
    
    this.render = function () {
        var templateFn;
        templateFn = doT.template(template.html());
        elements.panel.html(templateFn({
            score: model.getScore(),
            lastScore: model.lastScore,
            fourWays: model.fourWays
        }));
        elements.body.attr('class', model.getGameState());
    };
    
    this.unbindAll = function () {
        elements.controls.off('click');
    };
    
    this.render();
    
    elements.controls.on('click', '#new-game-button', function (e) {
        e.preventDefault();
        that.trigger('newGame');
    });

    elements.body.on('click', '.tutorial', function () {
        that.trigger('showTutorial');
    });
};

$.extend(ISH.StatusView.prototype, EventEmitter.prototype);
