/*global doT*/

var ISH = ISH || {};

ISH.PouchView = function (model, element, template) {
    'use strict';
    
    this.render = function () {
        var templateFn;
        templateFn = doT.template(template.html());
        element.html(templateFn({
            tile: model.getNextTile(),
            tilesLeft: model.getTilesLeft()
        }));
    };
};