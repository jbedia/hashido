/*global describe, expect, it, ISH, $*/

(function () {
    'use strict';

    function mockPouch(tiles) {
        var i, j, k, colour, symbol, newPouch;
        
        for (i = 0; i < tiles.length; i += 1) {
            colour = tiles[i].split(' ')[0];
            symbol = tiles[i].split(' ')[1];
            tiles[i] = new ISH.Tile(colour, symbol);
        }
        
        newPouch = new ISH.Pouch(ISH.colours, ISH.symbols, ISH.multiples, ISH.Tile);
        newPouch.setAllTiles(tiles);
        return newPouch;
    }
    
    function mockBoard(tiles) {
        var i, j, k, colour, symbol, tile,
            board = new ISH.Board(12, 8);
        
        for (k in tiles)  {
            if (tiles.hasOwnProperty(k)) {
                i = +k.split(',')[0];
                j = +k.split(',')[1];
                colour = tiles[k].split(' ')[0];
                symbol = tiles[k].split(' ')[1];
                tile = new ISH.Tile(colour, symbol);
                board.setTile(tile, i + j * 12);
            }
        }

        return board;
    }
    
    function coordToIndex(coord, width) {
        return +coord.split(',')[0] + coord.split(',')[1] * width;
    }

    describe('#ido game', function () {
        
        var ish = ISH.init({
            boardElement: $('<ol id="board"></ol>'),
            boardTemplate: $('<script id="board-template" type="text/x-dot-template">' +
                '{{~it.tiles :tile:index}}' +
                '<li class="cell {{?it.valid[index]}}valid{{??}}invalid{{?}}">' +
                '{{?tile}}' +
                '<span class="tile {{=tile.colour}} {{=tile.symbol}} {{?it.lastMove === index}}tile-new{{?}}">' +
                '{{=tile.colour}} {{=tile.symbol}}' +
                '</span>' +
                '{{?}}' +
                '</li>' +
                '{{~}}' +
                '</script>'),
            pouchElement: $('<div id="pouch"></div>'),
            pouchTemplate: $('<script id="pouch-template" type="text/x-dot-template">' +
                '{{?it.tile}}' +
                '<div id="next-tile" class="section">' +
                '<h1 class="label">Next tile</h1>' +
                '<div class="cell">' +
                '<span class="tile {{=it.tile.colour}} {{=it.tile.symbol}}">' +
                '{{=it.tile.colour}} {{=it.tile.symbol}}' +
                '</span>' +
                '</div>' +
                '</div>' +
                '{{?}}' +
                '<div id="tiles-left" class="section">' +
                '<h1 class="label">Tiles left</h1>' +
                '<span id="tiles-left-number" class="number">' +
                '{{=it.tilesLeft}}' +
                '</span>' +
                '</div>' +
                '</script>'),
            statusElements: {
                panel: $('<div id="status"></div>'),
                body: $('<body class="game-in-progress"></body>'),
                controls: $('<div id="controls">' +
                    '<div id="new-game">' +
                    '<button id="new-game-button">New game</button>' +
                    '</div>' +
                    '</div>'),
                footer: $('<footer id="page-footer">')
            },
            statusTemplate: $('<script id="status-template" type="text/x-dot-template">' +
                '<div id="score" class="section">' +
                '<h1 class="label">Score</h1>' +
                '<span id="score-number" class="number">' +
                '{{=it.score}}' +
                '</span>' +
                '</div>' +
                '</script>'),
            tutorialElement: $('<section id="tutorial" url="/tutorial.html"></section>')
        });
        
        describe('on startup', function () {
            var startingTiles, i, j, k, colours, symbols;
            
            it('should have a board, consisting of 96 squares', function () {
                expect(ish.board.getAllTiles()).to.be.an('array');
                expect(ish.board.getAllTiles().length).to.equal(96);
            });
    
            it('should lay out the board with 6 tiles, one in each corner and two in the centre, covering all colours and symbols between them', function () {
                startingTiles = [
                    ish.board.getTile(0),
                    ish.board.getTile(11),
                    ish.board.getTile(41),
                    ish.board.getTile(54),
                    ish.board.getTile(84),
                    ish.board.getTile(95)
                ];
                for (i = 0; i < startingTiles.length; i += 1) {
                    expect(startingTiles[i]).to.exist;
                }
                colours = {};
                symbols = {};
                for (i = 0; i < startingTiles.length; i += 1) {
                    if (colours[startingTiles[i].colour]) {
                        colours[startingTiles[i].colour] += 1;
                    } else {
                        colours[startingTiles[i].colour] = 1;
                    }
                    if (symbols[startingTiles[i].symbol]) {
                        symbols[startingTiles[i].symbol] += 1;
                    } else {
                        symbols[startingTiles[i].symbol] = 1;
                    }
                }
                for (k in colours) {
                    expect(colours[k]).to.equal(1);
                }
                for (k in symbols) {
                    expect(symbols[k]).to.equal(1);
                }
            });
            
            it('should have a pouch which (now) contains 66 pieces, two of each permutation of colour and symbol', function () {
                function count(colour, symbol) {
                    var tiles = ish.pouch.getAllTiles().concat(startingTiles),
                        c = 0,
                        i;
                    for (i = 0; i < tiles.length; i += 1) {
                        if (tiles[i].colour === colour && tiles[i].symbol === symbol) {
                            c += 1;
                        }
                    }
                    return c;
                }
                
                expect(ish.pouch.getTilesLeft()).to.equal(66);
                expect(ish.pouch.getAllTiles().length).to.equal(66);
                
                for (i = 0; i < ish.constants.colours.length; i += 1) {
                    for (j = 0; j < ish.constants.symbols.length; j += 1) {
                        expect(count(ish.constants.colours[i], ish.constants.symbols[i])).to.equal(ish.constants.multiples);
                    }
                }
            });
            
            it('should have a score of zero', function () {
                expect(ish.status.getScore()).to.equal(0);
            });
        });
        
        describe('valid moves', function () {
            var board,
                pouch = mockPouch(['red circle']);
           
            it('0 adjacent tiles: invalid', function () {
                board = mockBoard({});
                ish.game.calculateValidMoves(board, pouch);
                expect(board.getValid(coordToIndex('1,1', ish.constants.width))).to.be.false;
            });
                
            it('1 adjacent tile, matching in colour and symbol: valid', function () {
                board = mockBoard({
                    '1,1': 'red circle'
                });
                ish.game.calculateValidMoves(board, pouch);
                expect(board.getValid(coordToIndex('2,1', ish.constants.width))).to.be.true;
            });
                
            it('1 adjacent tile, matching in colour only: valid', function () {
                board = mockBoard({
                    '1,1': 'red diamond'
                });
                ish.game.calculateValidMoves(board, pouch);
                expect(board.getValid(coordToIndex('2,1', ish.constants.width))).to.be.true;
            });
                
            it('1 adjacent tile, matching in symbol only: valid', function () {
                board = mockBoard({
                    '1,1': 'orange circle'
                });
                ish.game.calculateValidMoves(board, pouch);
                expect(board.getValid(coordToIndex('2,1', ish.constants.width))).to.be.true;
            });
                
            it('1 adjacent tile, matching in neither colour nor symbol: invalid', function () {
                board = mockBoard({
                    '1,1': 'orange diamond'
                });
                ish.game.calculateValidMoves(board, pouch);
                expect(board.getValid(coordToIndex('2,1', ish.constants.width))).to.be.false;
            });
                
            it('2 adjacent tiles, one matching in colour and symbol, other neither: invalid', function () {
                board = mockBoard({
                    '1,1': 'red circle',
                    '2,2': 'orange diamond'
                });
                ish.game.calculateValidMoves(board, pouch);
                expect(board.getValid(coordToIndex('2,1', ish.constants.width))).to.be.false;
            });
            
            it('2 adjacent tiles, one matching in colour only, other in symbol only: valid', function () {
                board = mockBoard({
                    '1,1': 'red diamond',
                    '2,2': 'orange circle'
                });
                ish.game.calculateValidMoves(board, pouch);
                expect(board.getValid(coordToIndex('2,1', ish.constants.width))).to.be.true;
            });
                            
            it('2 adjacent tiles, both matching in colour only: invalid', function () {
                board = mockBoard({
                    '1,1': 'red diamond',
                    '2,2': 'red triangle'
                });
                ish.game.calculateValidMoves(board, pouch);
                expect(board.getValid(coordToIndex('2,1', ish.constants.width))).to.be.false;
            });
                                            
            it('2 adjacent tiles, both matching in symbol only: invalid', function () {
                board = mockBoard({
                    '1,1': 'orange circle',
                    '2,2': 'yellow circle'
                });
                ish.game.calculateValidMoves(board, pouch);
                expect(board.getValid(coordToIndex('2,1', ish.constants.width))).to.be.false;
            });
                
            it('3 adjacent tiles, 2 matching by colour, 1 by symbol: valid', function () {
                board = mockBoard({
                    '1,1': 'red diamond',
                    '2,2': 'orange circle',
                    '3,1': 'red triangle'
                });
                ish.game.calculateValidMoves(board, pouch);
                expect(board.getValid(coordToIndex('2,1', ish.constants.width))).to.be.true;
            });
                        
            it('3 adjacent tiles, 2 matching by symbol, 1 by colour: valid', function () {
                board = mockBoard({
                    '1,1': 'red diamond',
                    '2,2': 'orange circle',
                    '3,1': 'yellow circle'
                });
                ish.game.calculateValidMoves(board, pouch);
                expect(board.getValid(coordToIndex('2,1', ish.constants.width))).to.be.true;
            });
            
            it('3 adjacent tiles, 3 matching by colour, none by symbol: invalid', function () {
                board = mockBoard({
                    '1,1': 'red diamond',
                    '2,2': 'red triangle',
                    '3,1': 'red crescent'
                });
                ish.game.calculateValidMoves(board, pouch);
                expect(board.getValid(coordToIndex('2,1', ish.constants.width))).to.be.false;
            });
            
            it('3 adjacent tiles, 3 matching by symbol, none by colour: invalid', function () {
                board = mockBoard({
                    '1,1': 'orange circle',
                    '2,2': 'yellow circle',
                    '3,1': 'green circle'
                });
                ish.game.calculateValidMoves(board, pouch);
                expect(board.getValid(coordToIndex('2,1', ish.constants.width))).to.be.false;
            });
            
            it('3 adjacent tiles, 1 matching by colour and symbol, 1 by colour only, one by neither: invalid', function () {
                board = mockBoard({
                    '1,1': 'red circle',
                    '2,2': 'red diamond',
                    '3,1': 'orange diamond'
                });
                ish.game.calculateValidMoves(board, pouch);
                expect(board.getValid(coordToIndex('2,1', ish.constants.width))).to.be.false;
            });
            
            it('4 adjacent tiles, 2 matching by colour, 2 by symbol: valid', function () {
                board = mockBoard({
                    '1,1': 'red diamond',
                    '2,2': 'red triangle',
                    '3,1': 'orange circle',
                    '2,0': 'yellow circle'
                });
                ish.game.calculateValidMoves(board, pouch);
                expect(board.getValid(coordToIndex('2,1', ish.constants.width))).to.be.true;
            });
            
            it('4 adjacent tiles, 3 matching by colour, 1 by symbol: invalid', function () {
                board = mockBoard({
                    '1,1': 'red diamond',
                    '2,2': 'red triangle',
                    '3,1': 'red crescent',
                    '2,0': 'orange circle'
                });
                ish.game.calculateValidMoves(board, pouch);
                expect(board.getValid(coordToIndex('2,1', ish.constants.width))).to.be.false;
            });
            
            it('4 adjacent tiles, 3 matching by symbol, 1 by colour: invalid', function () {
                board = mockBoard({
                    '1,1': 'red diamond',
                    '2,2': 'orange circle',
                    '3,1': 'yellow circle',
                    '2,0': 'green circle'
                });
                ish.game.calculateValidMoves(board, pouch);
                expect(board.getValid(coordToIndex('2,1', ish.constants.width))).to.be.false;
            });
            
            it('4 adjacent tiles, 1 matching by colour and symbol, 1 by symbol, 1 by colour, 1 by neither: invalid', function () {
                board = mockBoard({
                    '1,1': 'red circle',
                    '2,2': 'red diamond',
                    '3,1': 'orange circle',
                    '2,0': 'orange diamond'
                });
                ish.game.calculateValidMoves(board, pouch);
                expect(board.getValid(coordToIndex('2,1', ish.constants.width))).to.be.false;
            });
            
        });
        
        describe('scoring', function () {
            
            it('1-way match should score 1*multiplier', function () {
                var status = new ISH.Status(),
                    board = mockBoard({
                    '1,1': 'red circle',
                    '2,1': 'red circle'
                });
                expect(ish.game.addScore(coordToIndex('2,1', ish.constants.width), board, status)).to.equal(1);
            });
            
            it('2-way match should score 2*multiplier', function () {
                var status = new ISH.Status(),
                    board = mockBoard({
                    '1,1': 'red diamond',
                    '2,2': 'orange circle',
                    '2,1': 'red circle'
                });
                expect(ish.game.addScore(coordToIndex('2,1', ish.constants.width), board, status)).to.equal(2);
            });
            
            it('3-way match should score 4*multiplier', function () {
                var status = new ISH.Status(),
                    board = mockBoard({
                    '1,1': 'red diamond',
                    '2,2': 'orange circle',
                    '3,1': 'red diamond',
                    '2,1': 'red circle'
                });
                expect(ish.game.addScore(coordToIndex('2,1', ish.constants.width), board, status)).to.equal(4);
            });
            
            it('4-way match should score 8*multiplier plus the next bonus, and double the multiplier', function () {
                var status = new ISH.Status(),
                    board = mockBoard({
                    '1,1': 'red diamond',
                    '2,2': 'orange circle',
                    '3,1': 'red diamond',
                    '2,0': 'orange circle',
                    '2,1': 'red circle',
                        
                    '4,1': 'red diamond',
                    '5,2': 'orange circle',
                    '6,1': 'red diamond',
                    '5,0': 'orange circle',
                    '5,1': 'red circle', 
                });
                expect(ish.game.addScore(coordToIndex('2,1', ish.constants.width), board, status)).to.equal(8 + ish.constants.bonuses[0]);
                expect(status.fourWays).to.equal(1);
                
                expect(ish.game.addScore(coordToIndex('5,1', ish.constants.width), board, status)).to.equal(8 * 2 + ish.constants.bonuses[1]);
                expect(status.fourWays).to.equal(2);
            });
            
        });
    });
}());
